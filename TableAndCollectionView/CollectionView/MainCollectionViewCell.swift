//
//  MainCollectionViewCell.swift
//  TableAndCollectionView
//
//  Created by Сергей Рунович on 10.02.21.
//

import UIKit

class MainCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var nameImageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configure(result: CellData) {
     nameImageLabel.text = result.name
     cellImageView.image = UIImage(named: result.imageNamed)
        
    }
    func setSelectedAppearence(isSelected: Bool) {
        if isSelected {
            cellImageView.alpha = 0.5
            
        } else {
            cellImageView.alpha = 1
           
        }
    }

}
