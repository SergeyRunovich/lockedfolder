//
//  CustomImageView.swift
//  TableAndCollectionView
//
//  Created by Сергей Рунович on 11.02.21.
//

import UIKit

class CustomView: UIView {

    @IBOutlet weak var scrollViewCustom: UIScrollView!
    @IBOutlet weak var imageViewCustom: UIImageView!
    
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = .black
        scrollViewCustom.delegate = self
    }
    
 
}

extension CustomView: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageViewCustom
    }

}
