//
//  ViewController.swift
//  TableAndCollectionView
//
//  Created by Сергей Рунович on 10.02.21.
//

import UIKit
import KeychainSwift
enum DataType {
    case folder
    case image
    case lockFolder
}

struct CellData {
    var name: String?
    var imageNamed: String
    var type: DataType
}
enum PresentationStyle {
    case tableView
    case collectionView
}
class ViewController: UIViewController {
   
    
    //MARK:- Properties -
    
    var folderData: [CellData] = []
    var folderNames: [String] = []
    let fileManager = FileManager.default
    var directoryURL: URL?
    var selectedObjects: [CellData] = []
    let notificationIdentifierArray = ["notification","notification1","notification2","notification3","notification4","notification5",]
    let keychain = KeychainSwift()
    var presentationStyle: PresentationStyle = PresentationStyle.tableView
    
    @IBOutlet weak var lockedFolderButton: UIBarButtonItem!
    @IBOutlet weak var addFolderAndImageButton: UIBarButtonItem!
    @IBOutlet weak var tableAndCollectionUiButton: UIBarButtonItem!
    @IBOutlet weak var editSettingButton: UIBarButtonItem!
    //MARK:- IBOutlets -
    @IBOutlet weak var mainTableView: UITableView! {
        didSet {
            mainTableView.delegate = self
            mainTableView.dataSource = self
            let nib = UINib(nibName: TableViewRegisterIdentifier.nibNameMainTableView.rawValue, bundle: nil)
            let newNib = UINib(nibName: TableViewRegisterIdentifier.nibNameimageTableView.rawValue, bundle: nil)
            mainTableView.register(nib, forCellReuseIdentifier: TableViewRegisterIdentifier.mainTableViewIdentifier.rawValue)
            mainTableView.register(newNib, forCellReuseIdentifier: TableViewRegisterIdentifier.imageTableViewIdentifier.rawValue)
            mainTableView.rowHeight = 100
            mainTableView.tableFooterView = UIView()
            let lockedNib = UINib(nibName: "LockedFolderTableView", bundle: nil)
            mainTableView.register(lockedNib, forCellReuseIdentifier: LockedFolderTableView.reuseIdentifier)
        }
    }
    
    @IBOutlet weak var mainCollectionView: UICollectionView! {
        didSet {
            mainCollectionView.delegate = self
            mainCollectionView.dataSource = self
            let nib = UINib(nibName: "MainCollectionViewCell", bundle: nil)
            mainCollectionView.register(nib, forCellWithReuseIdentifier: "mainCollCell")
            let lockedNib = UINib(nibName: "LockedFolderCollectionView", bundle: nil)
            mainCollectionView.register(lockedNib, forCellWithReuseIdentifier: LockedFolderCollectionView.reuseIdentifier)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lockedFolderButton.isEnabled = false
        mainCollectionView.allowsMultipleSelection = true
        editSettingButton.title = "Edit"
        if directoryURL == nil {
            directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        }
        tableAndCollectionUiButton.image = UIImage(systemName: "table")
        getDocuments(nameOfFolder: nil, folderUrls: directoryURL!)
        createNotifictaion()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Functions -
    func addAlert() {
        let alert = UIAlertController(title: "Add new folder", message: "", preferredStyle: .alert)
        alert.addTextField { (userNameField) in
            userNameField.font = .systemFont(ofSize: 17)
            userNameField.placeholder = "Name of folder"
            
            let okAction = UIAlertAction(title: "Ok", style: .default) { (_) in
                guard let textField = userNameField.text else { return }
                if (userNameField.text!.isEmpty)  {
                    self.showNotCorrectAlert()
                    return
                } else {
                    self.createDirectory(nameOfFolder: textField, FoldersUrl: self.directoryURL!)
                    self.getDocuments(nameOfFolder: textField, folderUrls: self.directoryURL!)
                }
            }
            alert.addAction(okAction)
        }
        let cancelACtion = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(cancelACtion)
        present(alert, animated: true, completion: nil)
    }
    
    func showNotCorrectAlert() {
        showAlert(title: "Error", message: "Name of folder empty", actionTitles: ["Try again"], actions: [{action1 in
            self.addAlert()
        }])
    }
    
    func createDirectory(nameOfFolder : String, FoldersUrl: URL) {
        let directoryURL = FoldersUrl
        do {
            let contentOfDirectory = try fileManager.contentsOfDirectory(at: directoryURL, includingPropertiesForKeys: nil)
            let newFolderUrl = directoryURL.appendingPathComponent(nameOfFolder)
            try fileManager.createDirectory(at: newFolderUrl, withIntermediateDirectories: false, attributes: nil)
            
        } catch {
            print("something went wrong")
        }
    }
    func getDocuments(nameOfFolder: String?, folderUrls: URL) {
      var directoryURL = folderUrls
        do {
            folderData = []
            folderNames = try fileManager.contentsOfDirectory(at: directoryURL, includingPropertiesForKeys: nil).map {$0.lastPathComponent}
            for i in 0..<folderNames.count {
                if folderNames[i].contains(".jpeg") {
                    
                    self.folderData.append(CellData(name: folderNames[i], imageNamed: "Image", type: .image))
                } else  {
                    self.folderData.append(CellData(name: folderNames[i], imageNamed: "Folder", type: .folder))
//                    self.folderData.append(CellData(name: folderNames[i], imageNamed: "lockFolder", type: .lockFolder))
                }
                folderData.sort(by: {$0.name! < $1.name!})
                
            }
            mainTableView.reloadData()
            mainCollectionView.reloadData()
        } catch  {
            print("Something went wrong")
        }
    }
    func newAlert() {
        showAlert(title: "Select an option", message: nil, actionTitles: ["Создать папку","Добавить Фото"], actions:[{action1 in
            self.addAlert()
        },{action2 in
            self.createImagePicker()
        }])
    }
    func deleteDocuments(nameOfFolder: String , folderUrl: URL) {
        
        let directoryURL = folderUrl
        
        do {
            let contentOfDirectory = try fileManager.contentsOfDirectory(at: directoryURL, includingPropertiesForKeys: nil)
            let newFolderUrl = directoryURL.appendingPathComponent(nameOfFolder)
            try fileManager.removeItem(at: newFolderUrl)
            
        } catch  {
            print("something wrong")
        }
    }
    
    func createImagePicker() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK:- IBActions-
    
    @IBAction func lockedFolderPress(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let lockAction = UIAlertAction(title: "Защитить \(selectedObjects.count) объект. паролем", style: .default) { (_) in
            
            let alertController = UIAlertController(title: "Задайте пароль для выбранных объектов", message: nil, preferredStyle: .alert)
            
            alertController.addTextField { (textField) in
                textField.text = "введите пароль"
                textField.clearsOnBeginEditing = true
                textField.isSecureTextEntry = true
            }
            
            let okAction = UIAlertAction(title: "Oк", style: .cancel) {_ in
                if let textFields = alertController.textFields {
                    for object in self.selectedObjects {
                        if let objectName = object.name,
                           let indexOfObject = self.folderData.firstIndex(where: { $0.name == objectName && $0.type == object.type}) {
                            self.folderData[indexOfObject].type = .lockFolder
                            self.keychain.set(textFields[0].text!, forKey: objectName)
                        }
                    }
                }
                self.mainTableView.reloadData()
                self.mainCollectionView.reloadData()
//                self.updateData()
//                self.makeDeletingDisabled()
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        alertController.addAction(lockAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
        
    }
    @IBAction func addButtonPress(_ sender: UIBarButtonItem) {
        newAlert()
    }
    @IBAction func changeViewPressButton(_ sender: UIBarButtonItem) {
        if mainTableView.alpha == 0 {
            tableAndCollectionUiButton.image = UIImage(systemName: "list.dash")
            mainTableView.alpha = 1
            mainCollectionView.alpha = 0
            mainTableView.reloadData()
            mainCollectionView.reloadData()
        } else {
            tableAndCollectionUiButton.image = UIImage(systemName: "table")
            mainTableView.alpha = 0
            mainCollectionView.alpha = 1
            mainTableView.reloadData()
            mainCollectionView.reloadData()
        }
    }
    @objc func addFolders() {
        newAlert()
    }
    
    @objc func deleteSelectedItems() {
        for objects in selectedObjects {
            if let directoryURL = directoryURL,
               let objectName = objects.name {
                do {
                    try fileManager.removeItem(at: directoryURL.appendingPathComponent(objectName))
                    folderData.removeAll(where: {$0.name == objects.name})
                    selectedObjects.removeAll(where: {$0.name == objects.name})
                } catch {
                    print("something went wrong")
                }
            }
        }
        mainCollectionView.reloadData()
        mainTableView.reloadData()
        editSettingButton.title = "Edit"
        tableAndCollectionUiButton.isEnabled = true
        addButton()
    }
    func addButton() {
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addFolders))
        navigationItem.rightBarButtonItem = addButton
    }
    func deleteButton() {
        let deleteButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteSelectedItems))
        navigationItem.rightBarButtonItem = deleteButton
    }
    
    func changeButton() {
        let changeButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: nil)
        navigationItem.rightBarButtonItem = changeButton
    }
    
    @IBAction func editButtonPress(_ sender: UIBarButtonItem) {
        if editSettingButton.title == "Edit" {
            sender.title = "Cancel"
            lockedFolderButton.tintColor = .black
            lockedFolderButton.isEnabled = true
            deleteButton()
            tableAndCollectionUiButton.isEnabled = false
        } else {
            sender.title = "Edit"
            addButton()
            tableAndCollectionUiButton.isEnabled = true
            lockedFolderButton.isEnabled = false
            mainTableView.reloadData()
            mainCollectionView.reloadData()
        }
    }
    
    func createNotifictaion() {
        let notificationCenter = UNUserNotificationCenter.current()
        
        notificationCenter.requestAuthorization(options: [.badge, .alert, .sound]) { (isAutorized, error) in
            if isAutorized {
                let content = UNMutableNotificationContent()
                content.body = "Добавь фото в защищенную папку"
                
                for identifier in 0..<self.notificationIdentifierArray.count {
                    let identifier = self.notificationIdentifierArray[identifier]
                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 60 * 60, repeats: true)
                    
                    let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
                    
                    notificationCenter.add(request) { (error) in
                        if let error = error {
                            print(error)
                        }
                    }
                }
            } else {
                if let error = error {
                    print(error)
                }
            }
        }
    }
}

//MARK:- ImagePickerDelegateExtension -
extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let originalImage = info[.originalImage] as? UIImage,
           let url = info[.imageURL] as? URL {
            if let newUrl = directoryURL {
                let directoryUrl = newUrl
                
                
                do {
                    let newImageUrl = (directoryUrl.appendingPathComponent(url.lastPathComponent))
                    
                    let pngImageData = originalImage.pngData()
                    
                    try pngImageData?.write(to: newImageUrl)
                } catch {
                    print("Something went wrong")
                }
            }
        }
        getDocuments(nameOfFolder: nil, folderUrls: directoryURL!)
        picker.dismiss(animated: true, completion: nil)
    }
    func showFolderController(_ currentData: CellData) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let folderViewController = storyboard.instantiateViewController(withIdentifier: "mainVC") as? ViewController,
           let folderName = currentData.name,
           let directoryURL = directoryURL {
            folderViewController.directoryURL = directoryURL.appendingPathComponent(folderName)
            folderViewController.presentationStyle = presentationStyle
            navigationController?.pushViewController(folderViewController, animated: true)
       
        }
    }

    func showLockedFolderController(_ currentData: CellData) {
        guard let folderName = currentData.name else {
            return
        }
        let alertController = UIAlertController(title: "Папка защищена!", message: "Введите пароль", preferredStyle: .alert)

        alertController.addTextField { (textField) in
            textField.text = ""
            textField.clearsOnBeginEditing = true
            textField.isSecureTextEntry = true
        }

        let okAction = UIAlertAction(title: "Oк", style: .cancel) {_ in
            if let textFields = alertController.textFields {
                if textFields[0].text! != self.keychain.get(folderName) {
                    let alertControllerError = UIAlertController(title: "Ошибка!", message: "Неверный пароль", preferredStyle: .alert)
                    let okErrorAction = UIAlertAction(title: "Ок", style: .cancel) { (_) in
                        self.present(alertController, animated: true, completion: nil)
                    }
                    alertControllerError.addAction(okErrorAction)
                    self.present(alertControllerError, animated: true, completion: nil)
                } else {
                    self.showFolderController(currentData)
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .default, handler: nil)

        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
}


//MARK:- TableViewExtension -
extension ViewController: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return folderData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentData = folderData[indexPath.row]
        switch currentData.type {
        case .image:
            if let imageCell = tableView.dequeueReusableCell(withIdentifier: TableViewRegisterIdentifier.imageTableViewIdentifier.rawValue, for: indexPath) as? ImageTableViewCell,
               let imageName = currentData.name {
                imageCell.nameImageLabel.text = currentData.name
                
                
                if let newUrl = directoryURL {
                    
                    
                    do {
                        let imageUrl = newUrl.appendingPathComponent(imageName)
                        imageCell.cellImageView.image = UIImage(contentsOfFile: imageUrl.path)
                    }
                    return imageCell
                }
            }
        case .folder:
            if let folderCell = tableView.dequeueReusableCell(withIdentifier: TableViewRegisterIdentifier.mainTableViewIdentifier.rawValue, for: indexPath) as? MainTableViewCell {
                folderCell.configure(result: folderData[indexPath.row])
                return folderCell
            }
        case .lockFolder:
            if let folderCell = tableView.dequeueReusableCell(withIdentifier: LockedFolderTableView.reuseIdentifier, for: indexPath) as? LockedFolderTableView {
                folderCell.folderTVCellLabel.text = currentData.name
                folderCell.setSelected(selectedObjects.contains(where: { $0.name == currentData.name}), animated: false)
               return folderCell
            }
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentData = folderData[indexPath.row]
        
        if editSettingButton.title == "Edit" {
            switch currentData.type {
            case .image:
                if let imageName = currentData.name {
                    let imageController = ImageViewContr(nibName: ImageViewContr.identifier, bundle: nil)
                    
                    if let newUrl = directoryURL {
                        
                        do {
                            
                            imageController.folderDirectoryURL = directoryURL
                            self.navigationController?.pushViewController(imageController, animated: true)
                        }
                    }
                }
            case .folder:
                
                let vc = storyboard?.instantiateViewController(identifier: "mainVC") as! ViewController
                let currentData = folderData[indexPath.row]
                if let folderName = currentData.name, let newDirectoryUrl = directoryURL {
                    do {
                        let folderUrl = newDirectoryUrl.appendingPathComponent(folderName)
                        vc.directoryURL = folderUrl
                    }
                }
                
                navigationController?.pushViewController(vc, animated: true)
           
            case .lockFolder:
                showLockedFolderController(currentData)
                
            }
            tableView.cellForRow(at: indexPath)?.isSelected = false
        } else {
            selectedObjects.append(currentData)
            mainCollectionView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let currentData = folderData[indexPath.row]
        
        if editSettingButton.title == "Cancel" {
            selectedObjects.removeAll(where: {$0.name == currentData.name})
            mainCollectionView.reloadData()
        }
    }
    
}
  



//MARK:- CollectionViewExtension -
extension ViewController: UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return folderData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let currentData = folderData[indexPath.row]
        switch currentData.type {
        case .image:
            if let imageCell = mainCollectionView.dequeueReusableCell(withReuseIdentifier: "mainCollCell", for: indexPath) as? MainCollectionViewCell,
               let imageName = currentData.name {
                imageCell.nameImageLabel.text = currentData.name
                
                imageCell.setSelectedAppearence(isSelected: selectedObjects.contains(where: { $0.name == currentData.name}))
                if let newUrl = directoryURL {
                    
                    
                    do {
                        let imageUrl = newUrl.appendingPathComponent(imageName)
                        
                        imageCell.cellImageView.image = UIImage(contentsOfFile: imageUrl.path)
                    }
                    return imageCell
                }
            }
        case .folder:
            if let folderCell = mainCollectionView.dequeueReusableCell(withReuseIdentifier: "mainCollCell", for: indexPath) as? MainCollectionViewCell {
                folderCell.configure(result: folderData[indexPath.row])
                folderCell.setSelectedAppearence(isSelected: selectedObjects.contains(where: { $0.name == currentData.name}))
                return folderCell
            }
        case .lockFolder:
            if let folderCell = collectionView.dequeueReusableCell(withReuseIdentifier: LockedFolderCollectionView.reuseIdentifier, for: indexPath) as? LockedFolderCollectionView {
                folderCell.folderCVCellLabel.text = currentData.name
                folderCell.setSelectedAppearence(isSelected: selectedObjects.contains(where: { $0.name == currentData.name}))
               return folderCell
            }

        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentData = folderData[indexPath.row]
        
        
        if editSettingButton.title == "Edit" {
            switch currentData.type {
            case .image:
                if let imageName = currentData.name {
                    let imageController = ImageViewContr(nibName: ImageViewContr.identifier, bundle: nil)
                    
                    if let newUrl = directoryURL {
                        
                        
                        
                        do {
                            imageController.folderDirectoryURL = directoryURL
                            self.navigationController?.pushViewController(imageController, animated: true)
                        }
                    }
                }
            
            case .folder:
                let vc = storyboard?.instantiateViewController(identifier: "mainVC") as! ViewController
                let currentData = folderData[indexPath.row]
                if let folderName = currentData.name, let newDirectoryUrl = directoryURL {
                    do {
                        let folderUrl = newDirectoryUrl.appendingPathComponent(folderName)
                        vc.directoryURL = folderUrl
                    }
                }
                
                navigationController?.pushViewController(vc, animated: true)
            case .lockFolder:
                showLockedFolderController(currentData)
                mainCollectionView.reloadData()
            }
    } else {
        if selectedObjects.contains(where: {$0.name == currentData.name}) {
            selectedObjects.removeAll(where: {$0.name == currentData.name})
        } else {
            selectedObjects.append(currentData)
        }
        mainCollectionView.reloadData()
    }
    
    mainCollectionView.deselectItem(at: indexPath, animated: true)
}

}
   
