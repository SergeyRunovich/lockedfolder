//
//  ImageTableViewCell.swift
//  TableAndCollectionView
//
//  Created by Сергей Рунович on 10.02.21.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var nameImageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            nameImageLabel.alpha = 0.5
            cellImageView.alpha = 0.5
        } else {
            nameImageLabel.alpha = 1
            cellImageView.alpha = 1
        }
       
    }
    
}
