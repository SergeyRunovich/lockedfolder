//
//  Enums + NameIdentifier.swift
//  TableAndCollectionView
//
//  Created by Сергей Рунович on 10.02.21.
//

import Foundation

enum TableViewRegisterIdentifier: String {
    case mainTableViewIdentifier = "mainCell"
    case nibNameMainTableView = "MainTableViewCell"
    case imageTableViewIdentifier = "imageCell"
    case nibNameimageTableView = "ImageTableViewCell"
}

//enum CollectionViewRegisterIdentifier: String {
//    case mainCollectionViewIdentifier = "mainCollCell"
//    case nibNameMainCollectionView = "MainCollectionViewCell"
//    
//}


