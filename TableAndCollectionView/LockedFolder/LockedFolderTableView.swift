//
//  LockedFolderTableView.swift
//  TableAndCollectionView
//
//  Created by Сергей Рунович on 12.02.21.
//

import UIKit

class LockedFolderTableView: UITableViewCell {

    static let reuseIdentifier = "lockedFolderCell"
    
    @IBOutlet weak var folderTVCellImageView: UIImageView!
    @IBOutlet weak var folderTVCellLabel: UILabel!
    @IBOutlet weak var lockTVCellImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            folderTVCellLabel.alpha = 0.5
            folderTVCellImageView.alpha = 0.5
            lockTVCellImageView.alpha = 0.5
        } else {
            folderTVCellLabel.alpha = 1
            folderTVCellImageView.alpha = 1
            lockTVCellImageView.alpha = 1
        }
    }
}
