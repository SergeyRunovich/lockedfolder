//
//  LockedFolderCollectionView.swift
//  TableAndCollectionView
//
//  Created by Сергей Рунович on 12.02.21.
//

import UIKit

class LockedFolderCollectionView: UICollectionViewCell {

    static let reuseIdentifier = "lockedFolderCell"
    
    @IBOutlet weak var folderCVCellLabel: UILabel!
    @IBOutlet weak var lockCVCellImageView: UIImageView!
    @IBOutlet weak var folderCVCellImageView: UIImageView!
    
    
    func setSelectedAppearence(isSelected: Bool) {
        if isSelected {
            folderCVCellLabel.alpha = 0.5
            folderCVCellImageView.alpha = 0.5
            lockCVCellImageView.alpha = 0.5
        } else {
            folderCVCellLabel.alpha = 1
            folderCVCellImageView.alpha = 1
            lockCVCellImageView.alpha = 1
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
