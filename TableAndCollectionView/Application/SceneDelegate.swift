//
//  SceneDelegate.swift
//  TableAndCollectionView
//
//  Created by Сергей Рунович on 10.02.21.
//

import UIKit
import KeychainSwift
import LocalAuthentication

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    let keychain = KeychainSwift()
    

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        let context = LAContext()
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Please authenticate to proceed.") { (success, error) in
                if success {
                    print("success")
                } else {
                    DispatchQueue.main.async {
                        guard let error = error else { return }
                        print(error.localizedDescription)
                        if self.keychain.get("password") == nil {
                            
                            let alertController = UIAlertController(title: "Приветствуем в PhotoStorage!", message: "Задайте свой пароль", preferredStyle: .alert)
                            
                            alertController.addTextField { (textField) in
                                textField.text = "введите пароль"
                                textField.clearsOnBeginEditing = true
                                textField.isSecureTextEntry = true
                            }
                            
                            alertController.addTextField { (textField) in
                                textField.text = "подтвердите пароль"
                                textField.clearsOnBeginEditing = true
                                textField.isSecureTextEntry = true
                            }
                            
                            let okAction = UIAlertAction(title: "Oк", style: .cancel) {_ in
                                if let textFields = alertController.textFields {
                                    if textFields[0].text! == textFields[1].text! {
                                        self.keychain.set(textFields[0].text!, forKey: "password")
                                    } else {
                                        let alertControllerError = UIAlertController(title: "Ошибка!", message: "Введите одинаковый пароль", preferredStyle: .alert)
                                        let okErrorAction = UIAlertAction(title: "Ок", style: .cancel) { (_) in
                                            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                                        }
                                        alertControllerError.addAction(okErrorAction)
                                        self.window?.rootViewController?.present(alertControllerError, animated: true, completion: nil)
                                    }
                                }
                            }
                            alertController.addAction(okAction)
                            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                        } else {
                            let alertController = UIAlertController(title: "Хранилище защищено!", message: "Введите пароль", preferredStyle: .alert)
                            
                            alertController.addTextField { (textField) in
                                textField.text = ""
                                textField.clearsOnBeginEditing = true
                                textField.isSecureTextEntry = true
                            }
                            
                            let okAction = UIAlertAction(title: "Oк", style: .cancel) {_ in
                                if let textFields = alertController.textFields {
                                    if textFields[0].text! != self.keychain.get("password") {
                                        let alertControllerError = UIAlertController(title: "Ошибка!", message: "Неверный пароль", preferredStyle: .alert)
                                        let okErrorAction = UIAlertAction(title: "Ок", style: .cancel) { (_) in
                                            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                                        }
                                        alertControllerError.addAction(okErrorAction)
                                        self.window?.rootViewController?.present(alertControllerError, animated: true, completion: nil)
                                    }
                                }
                            }
                            
                            alertController.addAction(okAction)
                            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
       
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }




}
